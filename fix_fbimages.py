#!/usr/bin/env python3.8

import time
import logging

from pathlib import Path
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

DIRECTORIES_TO_WATCH = [
    (Path("/home/qyv/Public/ViberBackups/Viber Images"), False),
    (Path("/home/qyv/Pictures/Pink_PhotoFrame"), False),
    (Path("/home/qyv/Downloads"), False),
]


class Watcher:
    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()

        for watchdir, recurse in DIRECTORIES_TO_WATCH:
            self.observer.schedule(
                event_handler=event_handler, path=str(watchdir), recursive=recurse,
            )

        self.observer.start()

        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("\n")

        self.observer.join()


class Handler(FileSystemEventHandler):
    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        path = Path(event.src_path)

        suffixes = (
            ".jpg",
            ".JPG",
            ".jpeg",
            ".JPEG",
        )

        if path.suffix in suffixes:

            if event.event_type == "created":

                from subprocess import run, PIPE

                cmdline = f"exiftool -q -P -overwrite_original "
                cmdline += f'-gps:all="" '
                cmdline += f'-SpecialInstructions=""  "{path}"'

                logging.debug(f"cmdline: {cmdline}")

                result = run(cmdline, shell=True, stdout=PIPE, stderr=PIPE)

                logging.info(f"Fixed: '{path}'")


if __name__ == "__main__":

    logging.basicConfig(level=logging.DEBUG, format="%(message)s")

    w = Watcher()
    w.run()
