Python script to check for JPEG images created in particular folders, 
which strips out the SpecialInstructions embedded by Facebook.

Makes use of the Python watchdog API in order to watch two specific
directories.